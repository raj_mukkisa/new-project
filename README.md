# -----

# This is a sample build configuration for PHP.
# Check our guides at https://confluence.atlassian.com/x/e8YWN for more examples.
# Only use spaces to indent your .yml configuration.
image: php:7.4-fpm

options:
  max-time: 10
pipelines:
  pull-requests:
    '**': #this runs as default for any branch not elsewhere defined
      - step:
          name: Node Build
          image: node:lts
          caches:
            - node
          script:
            - npm install
            - npm run dev
          artifacts:
            - public/css/**
            - public/fonts/**
            - public/img/**
            - public/js/**
      - step:
          name: App Build
          caches:
            - composer
          script:
            - >-
              apt-get update && apt-get install -qy
              git
              curl
              libmcrypt-dev
              mariadb-client
              ghostscript
              ssh rsync
              zlib1g-dev libzip-dev unzip
            - yes | pecl install mcrypt-1.0.3
            - docker-php-ext-install pdo_mysql bcmath exif zip
            - curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
            - cp .env.example .env
            - >-
              composer install &&
              php artisan clear-compiled &&
              php artisan config:clear &&
              php artisan route:clear &&
              php artisan migrate:fresh &&
              php artisan db:seed --class=PermissionSeeder &&
              php artisan optimize:clear
          artifacts:
            - vendor/**
            - .env
          services:
            - mariadb
      - parallel:
        - step:
            name: Run Cs Fixer
            script:
              - ./vendor/bin/php-cs-fixer fix --config=.phpcs_laravel.php -v --dry-run --using-cache=no
        - step:
            name: Run Unit Tests
            script:
              - ./vendor/bin/phpunit --testsuite Unit
        - step:
            name: Run Feature Tests
            script:
              - &run-nohup nohup php artisan serve > /dev/null 2>&1 &
              - ./vendor/bin/phpunit --testsuite Feature
definitions:
  services:
    mariadb:
      image: mariadb:10.3
      environment:
        MYSQL_DATABASE: 'invoicing'
        MYSQL_ROOT_PASSWORD: 'root'
        MYSQL_USER: 'invoicing'
        MYSQL_PASSWORD: 'root'